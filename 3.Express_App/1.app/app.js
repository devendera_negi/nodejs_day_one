const express = require('express');

const app = express();

app.get('/', (req, res) => {
    res.send({
        name: 'Devendra',
        age: 32
    })
});

app.listen(7777, () => {
    console.log('app running at port 7777');
});