const express = require('express');
// const fs = require('fs');
const app = express();

app.use(express.static(__dirname + '/public'));
/**
 * middleware and next function
 * http://expressjs.com/en/guide/using-middleware.html
 */
// app.use((req, res, next) => {
//  // will never go to next functin all
// });

// app.use((req, res, next) => {
//     next();
// });

/**
 * creating logs middleware
 */

// app.use((req, res, next) => {
//     const time = new Date().toString();
//     const log = `${time}:: ${req.method}:: ${req.url}`;
//     console.log(log);
//     next();    
// });



// app.use((req, res, next) => {
//     const time = new Date().toString();
//     const log = `${time}:: ${req.method}:: ${req.url}`;
//     console.log(log);
//     next();    
// });

/**
 * assignement -1
 */

// app.use((req, res, next) => {
//     const time = new Date().toString();
//     const log = `${time}:: ${req.method}:: ${req.url}`;
//     fs.appendFile('server.log', log + '\n', (err) => {
//        if(err) {
//            console.log('Unable to save log to files');
//        } 
//     });
//     next();    
// });


app.get('/data', (req, res) => {
    res.send({
        name: 'Devendra',
        age: 32
    })
});

app.listen(7777, () => {
    console.log('app running at port 7777');
});