// const request = require('request');

// const url = `https://api.darksky.net/forecast/14e3b8d1298a2a9e7ec77407f412ecad/37.8267,-122.4233`;
// request({
//     url,
//     json: true
// }, (error, response, body) => {
//     if(!error && response.statusCode === 200) {
//         console.log(body.currently.temperature);
//     } else {
//         console.log('Unable to fetch weather');
//     }        
// });


/**
 * create getWeateh function
 */

// const request = require('request');

// const getWeather = () => {
//     const url = `https://api.darksky.net/forecast/14e3b8d1298a2a9e7ec77407f412ecad/37.8267,-122.4233`;
//     request({
//         url,
//         json: true
//     }, (error, response, body) => {
//         if(!error && response.statusCode === 200) {
//             console.log(body.currently.temperature);
//         } else {
//             callback('Unable to fetch weather');
//         }        
//     })
// }

// module.exports.getWeather = getWeather;

/**
 * returning temperature from get weather
 */

const request = require('request');

const getWeather = (lat, lng, callback) => {
    const url = `https://api.darksky.net/forecast/14e3b8d1298a2a9e7ec77407f412ecad/${lat},${lng}`;
    request({
        url,
        json: true
    }, (error, response, body) => {
        if(!error && response.statusCode === 200) {
            callback(undefined,  {
                temperature: body.currently.temperature,
                apparentTemperature: body.currently.apparentTemperature
            });
        } else {
            callback('Unable to fetch weather');
        }        
    })
}

module.exports.getWeather = getWeather;