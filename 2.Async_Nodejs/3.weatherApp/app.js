// const request = require('request');
// const yargs = require('yargs');

// const geocode = require('./geocode');

// const argv = yargs
// .options({
//     address: {
//         demand: true,
//         describe: 'Address to fetch weather for',
//         string: true
//     }
// })
// .help()
// .alias('help', 'h')
// .argv;

// geocode.getAddress(argv.address, (err, res) => {
//     if(err) {
//         console.log(err);
//     } else {
//         console.log(JSON.stringify(res, undefined, 2));
//     }
// });


// const request = require('request');
// const yargs = require('yargs');

// const geocode = require('./geocode');
// const weather = require('./weather');

// const argv = yargs
// .options({
//     address: {
//         demand: true,
//         describe: 'Address to fetch weather for',
//         string: true
//     }
// })
// .help()
// .alias('help', 'h')
// .argv;

// weather.getWeather();

/**
 * calling get weather funciton
 */

const yargs = require('yargs');
const geocode = require('./geocode');
const weather = require('./weather');

const argv = yargs
.options({
    address: {
        demand: true,
        alsias: 'a',
        describe: 'Address to fetch weather for',
        string: true
    }
})
.help()
.alias('help', 'h')
.argv;

geocode.getAddress(argv.address, (errorMessage, results) => {
    if(errorMessage) {
        console.log(errorMessage);
    } else {
        console.log(JSON.stringify(results.address, undefined, 2));

        weather.getWeather(results.latitude, results.longitude, (errorMessage, results) => {
            if(errorMessage) {
                console.log(errorMessage);
            } else {
                console.log(JSON.stringify(results, undefined, 2));
            } 
        })
    }   
});