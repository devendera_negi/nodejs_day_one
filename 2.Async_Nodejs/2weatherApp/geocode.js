const request = require('request');


const getAddress = (address, callback) => {
    const encodedAddress = encodeURIComponent(address);
    const URL = `https://maps.googleapis.com/maps/api/geocode/json?key=AIzaSyAwKLUZrAp0T8HCMDHGqFn9nokZm4Jmi_4&address=${encodedAddress}`;
    request({
        url: URL,
        json: true
    }, (error, response, body) => {
        if(error) {
            callback('Unable to connect to Server');
        } else if(body.status === 'ZERO_RESULTS') {
            callback('Unable to find that address');
        } else if(body.status === 'OK') {
            callback(undefined, {
                address: body.results[0].formatted_address,
                latitude: body.results[0].geometry.location.lat,
                longitude: body.results[0].geometry.location.lng
            })
        }
        
    });
}


module.exports.getAddress = getAddress;