const request = require('request');
const yargs = require('yargs');

const geocode = require('./geocode');

const argv = yargs
.options({
    address: {
        demand: true,
        describe: 'Address to fetch weather for',
        string: true
    }
})
.help()
.alias('help', 'h')
.argv;

geocode.getAddress(argv.address, (err, res) => {
    if(err) {
        console.log(err);
    } else {
        console.log(JSON.stringify(res, undefined, 2));
    }
});