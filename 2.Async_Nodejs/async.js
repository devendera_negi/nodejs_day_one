/**
 * Example one
 */

//  console.log('App started');

//  setTimeout(() => {
//     console.log('set time out');
//  },200);

//  console.log('App completed');

/**
 *  Assignment one
 *  Set timeout with 0 delay
 */

 /**
  * Call back function example
  */

  const getAnimal = (type, callback) => {
      const cat = {name: 'lucy', type: 'cat'};
      const dob = {name: 'max', type: 'dog'};
      let animal;
      if(type === 'dog') {
        animal = dog;
      } else if(type === 'cat') {
          animal = cat;
      }
      callback(animal);
  };

  getAnimal('cat', (animalObj) => {
    console.log(animalObj);
  });

  /**
   * assignnment two 
   *  simulate delay like database call using settimeout
   */

//   const getAnimal = (type, callback) => {
//         const cat = {name: 'lucy', type: 'cat'};
//         const dob = {name: 'max', type: 'dog'};
//         let animal;
//         if(type === 'dog') {
//         animal = dog;
//         } else if(type === 'cat') {
//             animal = cat;
//         }

//         setTimeout(() => {
//             callback(animal);
//         },2000);

    
//     };

//     getAnimal('cat', (animalObj) => {
//         console.log(animalObj);
//     });