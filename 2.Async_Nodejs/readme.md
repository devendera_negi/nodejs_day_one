#8 Async
    1. settimeout example
    2. Event loop
    3. Callback function
    
#9 Npm Intor     
    1. npm commands 
    2. package json
    3. installing packages globaly
    4. git ignore

#10 Pre App setup
    1. google app api key 
    2. https://goo.gl/NkRPVK (https://developers.google.com/maps/documentation/javascript/)
    3. Chrome plugin to view json
#11 App setup
    1. setup project 
    2. install request npm package
    3. check for body and response
    4. show response on http://jsoneditoronline.org/
    5. make bed request (error handling)
#12 UserInput yargs
    1. install yargs
    2. yargs documention

#13 Handling Error Callback
    1. Machine error (network error)
    2. api error 
#14 refactoring App
    1. creating a new file geocode.js
    2. register with https://darksky.net/dev/login
    3. make a sample requet
    4. create a weather.js
    5. get temperature form hardcode URL
    6. integrating app

#15 Promises
    1. promise example
    2. convert weather app to Axio promise

