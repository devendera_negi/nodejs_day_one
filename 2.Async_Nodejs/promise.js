// var somePromise = new Promise((resolve, reject) => {
// 		resolve('Hey, It worked!');
// });

// somePromise.then((message) => {
//     console.log(message);
// });

/**
 * throw an exception
 */

/**
 * async  
 */
// var somePromise = new Promise((resolve, reject) => {
// 	setTimeout(() => {
// 		resolve('Hey, It worked!');
// 		// reject('woops');
// 	}, 2000);
// });

// somePromise.then((message) => {
//     console.log(message);
// },(e) => {
//     console.log(e);   
// });

/**
 * using catch helper function
 */

// var somePromise = new Promise((resolve, reject) => {
// 	setTimeout(() => {
// 		resolve('Hey, It worked!');
// 		// reject('woops');
// 	}, 2000);
// });

// somePromise.then((message) => {
//     console.log(message);
// })
// .catch(error => { console.log('caught', error); });


/**
 * 
 */
// var somePromise = new Promise((_, reject) => {
//     reject(new Error('woops'))
// });


// somePromise.then((message) => {
//     console.log(message);
// }).catch(error => { console.log('caught', err.message); });

// var somePromise = new Promise(() => { throw new Error('exception!'); });
/**
 * unhandledRejection handaling
 */


var somePromise = new Promise((_, reject) => {
    reject(new Error('woops'))
});


somePromise.then((message) => {
    console.log(message);
}).catch(error => { console.log('caught', err.message); });

process.on('unhandledRejection', error => {
    // Will print "unhandledRejection err is not defined"
    console.log('unhandledRejection', error.message);
  });