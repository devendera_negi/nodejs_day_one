#1 concept 
    1. global === window
    2. every nodejs file is a module
    3. variables don't add to global name space
    4. __dirname, __filename what else
    5. node --inspect-brk filename.js
    6. Chrome -> chrome://inspect/#devices

#2 Core modules
    1. path
    2. process object (document), documentation for more details
    3. process argv
    4. process stdout
    5. require function
    6. util module
    7. v8 module
    8. readline module
#3 Event 
    1. Event (Browser like implimnataion)
    2. Event emitter constructor 
    3. create a new event and emit
#4 Module Export
    1. Export module example
    2. More about arrow function, why not use arrow on event listner callback
    3. Different way of exporting modules
#5 File System
    1. https://nodejs.org/dist/latest-v8.x/docs/api/fs.html
    2. read directory 
    3. write file
#6 Https Module
    1. http read from public websiet (wikipedia)
    2. build server
    3. build a file server