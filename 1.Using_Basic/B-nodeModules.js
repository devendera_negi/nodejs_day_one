const path = require('path');

// console.log(`path of my file is ${path.basename(__filename)}`);
// console.log(process);
// console.log(process.argv);

// node app.js --user Dev --greeting "Good Morning"

// // TODO 
// const getUserInput = (flag) => {
//     const index = process.argv.indexOf(flag);
//     return (index == -1) ? null : process.argv[index + 1 ];
// }

// const greeting = getUserInput('--greeting');
// const user = getUserInput('--user');

// if(!user || !greeting) {
//     console.log("Greeting from Node!");
// } else {
//     console.log(`${user} says ${greeting}`);
// }


// var questions = [
//     "What is your name?",
//     "Are you a developer?",
//     "What is your preferred programming language?"
//   ];
  
//   var answers = [];
  
//   function ask(i) {
//     process.stdout.write(`\n\n\n\n ${questions[i]}`);
//     process.stdout.write("  >  ");
//   }
  
//   process.stdin.on('data', function(data) {
  
//       answers.push(data.toString().trim());
  
//       if (answers.length < questions.length) {
//           ask(answers.length);
//       } else {
//           process.exit();
//       }
  
//   });
  
//   process.on('exit', function() {
  
//       process.stdout.write("\n\n\n\n");
  
//       process.stdout.write(`${answers[1]}! You are a great  ${answers[2]} developer, ${answers[0]}`);
  
//       process.stdout.write("\n\n\n\n");
  
//   });
  
//   ask(0);


// =======================Util========================
const util = require('util');
const v8 = require('v8');

util.log(path.basename(__filename));
util.log(v8.getHeapSpaceStatistics());