// const http = require('http');

// const server = http.createServer((req, res) => {
//     res.writeHead(200, {'Content-Type': 'text/plain'});
//     res.end('Hello form server');
// });

// server.listen(3333);

// console.log("server is running on port 3333");


/**
 * server httml content
 */

const http = require("http");

http.createServer((req, res) =>{

   res.writeHead(200, {"Content-Type": "text/html"});
   res.end(`<!DOCTYPE html>
     <html>
       <head>
         <title>Web Server</title>
       </head>
       <body>
         <h1>Hello World</h1>
         <p>${req.url}</p>
         <p>${req.method}</p>
     </html> 
   `);

}).listen(3333);

console.log("Server running on port 3333");