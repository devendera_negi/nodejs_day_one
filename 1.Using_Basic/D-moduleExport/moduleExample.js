const add = (a, b) => a+b
// module.epxorts = add;
module.exports = { add };
console.log(module)

// (function (exports, require, module, __filename, __dirname) { 
//     module.epxorts.add = (a, b) => a+b
    
// });

/**
 * ### Module ####
 * 1. Variable “module” is an object representing the current module
 * 2. It is local to each module and it is also private
 */

 /**
  * Module.exports is the object reference that gets returned from the require() calls
  * It is automatically created by Node.js.
  * It is just a reference to a plain JavaScript object. 
  * It is also empty by default
  */

/**
 * It is common practice to replace module.exports with custom functions or objects.
 * If we do that but still would like to keep using the “exports” shorthand;
 * then “exports” must be re-pointed to our new custom object
 * 
 */

//   exports = module.exports = {}
//   exports.method = function() {
//       // your code
//   }