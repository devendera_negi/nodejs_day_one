const {ObjectID} = require('mongodb');

const Notes = require('../server/models/notes');
const User = require('../server/models/user');

const id = '5a276153e6cffd458908bbcd2';

// Notes.find({
//     _id: id
// }).then((notes) => {
//     console.log('Todos:-', JSON.stringify(notes, undefined, 2));
// });

// // find only one result
// Notes.findOne({
//     _id: id
// }).then((note) => {
//     console.log('note:-', JSON.stringify(note, undefined, 2));
// });

// // find elements by Id
// Notes.findById(id).then((note) => {
//    console.log('note by Id:-', JSON.stringify(note, undefined, 2)); 
// });

// Notes.findById(id).then((note) => {
//     if(!note) {
//         return console.log('Id not found');
//     }
//     console.log('note by Id:-', JSON.stringify(note, undefined, 2)); 
//  });

// // handling worng object id

// if(!ObjectID.isValid(id)) {
//     console.log('ID not valid');
// }
// Notes.findById(id).then((note) => {
//     if(!note) {
//         return console.log('Id not found');
//     }
//     console.log('note by Id:-', JSON.stringify(note, undefined, 2)); 
//  }).catch((e) => console.log(e));



// // find user
    User.findById('5a25ffb315bb652fea81094e')
        .then((user) => {
            if(!user) {
                return console.log('Unable to find user');
            }
            console.log(JSON.stringify(user, undefined, 2));
        }, (e) => {
           console.log(e); 
        });