// const MongoClient = require('mongodb').MongoClient;
const { MongoClient, ObjectID } = require('mongodb');

const obj = new ObjectID();
    
// MongoClient.connect('mongodb://localhost:27017/mynotes', (err, db) => {
MongoClient.connect('mongodb://devnegi:test123test@ds129386.mlab.com:29386/funchar', (err, db) => {
    if(err) {
        return console.log('Unable to connect to MongoDB server');        
    }
    console.log('connected to MongoDB server');
   
    // db.collection('notes')
    //     .findOneAndUpdate({
    //         _id: new ObjectID('5a25ebafead708206b4be261')
    //     }, {
    //         $set: {
    //             status: false
    //         }
    //     }, {
    //         returnOriginal: false
    //     }).then((result) => {
    //         console.log(JSON.stringify(result, undefined, 2));            
    //     })

    // db.collection('user')
    // .findOneAndUpdate({
    //     _id: new ObjectID('5a25f4049dd134278a06ee7a')
    // }, {
    //     $inc: {
    //         age: 1
    //     }
    // }, {
    //     returnOriginal: false
    // }).then((result) => {
    //     console.log(JSON.stringify(result, undefined, 2));            
    // })

    db.collection('user')
    .findOneAndUpdate({
        _id: new ObjectID('5a25f4049dd134278a06ee7a')
    }, {
        $inc: {
            age: 1
        }
    }, {
        returnOriginal: false
    }).then((result) => {
        console.log(JSON.stringify(result, undefined, 2));            
    })

    db.close();
});