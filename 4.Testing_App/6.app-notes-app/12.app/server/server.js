const express = require('express');
const bodyParser = require('body-parser');
const { ObjectID } = require('mongodb');

const  mongoose  = require('./db/connect');
const  Notes = require('./models/notes');
const  User = require('./models/user');

const app = express();
// const port = process.evn.PORT || 7777;
const port = 7777;

app.use(bodyParser.json());

app.post('/notes', (req, res) => {
    const notes = new Notes({
        text: req.body.text,
        description: req.body.description
    });
    
    notes.save().then((doc) => {
        res.send(doc);  
    }, (e) => {
        res.status(400).send(e);
    })
});

// Get all notes

app.get('/notes', (req, res) => {
    Notes.find().then((notes) => {
        res.send({notes, status: 'success'});
    }, (e) => {
        res.status(400).send(e);
    })
});

// Get with id
app.get('/notes/:id', (req, res) => {
    // res.send(req.params);
    // check for valid Id
    // send back 400 with empty body
    // Check with find by Id
    // if success -> send note, or error
    // error -400 with empty body
    const id = req.params.id;

    if(!ObjectID.isValid(id)) {
        return res.status(404).send()
    }
    Notes.findById(id).then((note) => {
        if(!note) {
            return res.status(404).send();
        }
        res.send({note, status: 'success'});
    }).catch((e) => {
        res.status(404).send();
    })

});

app.delete('/notes/:id', (req, res) => {
    // check for valid Id
    // send back 400 with empty body
    // remove by Id
    // if success -> send back note with 200, if no doc send 404
    // error -400 with empty body
    const id = req.params.id;
    
    if(!ObjectID.isValid(id)) {
        return res.status(404).send()
    }

    Notes.findByIdAndRemove(id).then((note) => {
        if(!note) {
            return res.status(404).send();
        }
        res.send({note, status: 'success'});
    }).catch((e) => {
        res.status(404).send();
    });

})
app.listen(port, () => {
    console.log(`Server running on port ${port}`);
});

module.exports = app;