const mongoose  = require('../db/connect');

const Notes = mongoose.model('Notes', {
    text: {
        type: String,
        require: true,
        minlength: 3,
        trim: true
    },
    description: {
        type: String,
        minlength: 10,
        trim: true
    },
    status: {
        type: Boolean,
        default: true
    },
    doneAt: {
        type: Number,
        default: null
    }
});

// const myNote = new Notes({
//     text: 'aa test',
//     description: 'More item sto set this'
// })

// const myNote = new Notes({
//     text: 797,
//     description: 'More item sto set this'
// })

// myNote.save().then((doc)=>{
//     console.log('Notes saved', doc);
    
// }, (e)=> {
//     console.log('Unable to save Notes', e);
// });

module.exports = Notes;