// const MongoClient = require('mongodb').MongoClient;
const { MongoClient, ObjectID } = require('mongodb');

const obj = new ObjectID();

MongoClient.connect('mongodb://devnegi:test123test@ds129386.mlab.com:29386/funchar', (err, db) => {
    if(err) {
        return console.log('Unable to connect to MongoDB server');        
    }
    console.log('connected to MongoDB server');
    
    db.collection('notes').insertOne({
        text: 'Some notes',
        description: 'my dies',
        status: true
    }, (err, result) => {
        if(err) {
            return console.log('Unable to insert to DB', err);
        }
        console.log(JSON.stringify(result.ops, undefined, 2));        
    });

    db.collection('user').insertOne({
        name: 'Dev Negi',
        email: 'devendera.negi@gmail.com',
        status: true
    }, (err, result) => {
        if(err) {
            return console.log('Unable to insert to DB', err);
        }
        // console.log(JSON.stringify(result.ops, undefined, 2));
        // get time stamp from created record
        console.log(result.ops[0]._id.getTimestamp());
        
    });

    db.close();
});