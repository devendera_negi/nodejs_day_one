const express = require('express');
const bodyParser = require('body-parser');

const { mongoose }  = require('./db/connect');
const { Notes } = require('./models/notes');
const { User } = require('./models/notes');

const app = express();

app.use(bodyParser.json());

app.post('/notes', (req, res) => {
    console.log(req.body);
});

app.listen(3333, () => {
    console.log('Server running on port 3000');
});