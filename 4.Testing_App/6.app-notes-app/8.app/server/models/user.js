const mongoose  = require('../db/connect');

const User = mongoose.model('User', {
    name: {
        type: String,
        require: true,
        minlength: 3,
        trim: true
    },
    email: {
        type: String,
        require: true,
        trim: true
    },
    status: {
        type: Boolean,
        default: true
    },
    age: {
        type: Number,
        default: null
    }
});

// const newUSer = new User({
//     name: 'Bala S',
//     email: 'sbala@gmail.com',
//     age: 27
// })

// const myNote = new Notes({
//     text: 797,
//     description: 'More item sto set this'
// })

// newUSer.save().then((doc)=>{
//     console.log('new user saved', doc);
    
// }, (e)=> {
//     console.log('Unable to save users', e);
// });

module.exports = User;