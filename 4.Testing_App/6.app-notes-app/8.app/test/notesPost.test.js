const expect = require('expect');
const request = require('supertest');

const app = require('../server/server');
const Notes = require('../server/models/notes');

beforeEach((done) => {
    Notes.remove({}).then(() => done());
});

describe('POST /notes', () => {
    it('should create a new note', (done) => {
        const text = 'My note test';
        request(app)
            .post('/notes')
            .send({text})
            .expect(200)
            .expect((res) => {
                expect(res.body.text).toBe(text);
            })
            .end((err, res) => {
                if(err) {
                    return done(err);
                }
                Notes.find().then((note) => {
                    expect(note.length).toBe(1);
                    expect(note[0].text).toBe(text);
                    done();
                }).catch((e) => done(e));
        });
    });

    // it('should not create notes with invalid data', (done) => {
    //     request(app)
    //         .post('./notes')
    //         .send({text: ''})
    //         .expect(400)
    //         .end((err, res) => {
    //             if(err) {
    //                 return done(err);
    //             }

    //             Notes.find().then((note) => {
    //                 expect(note.length).toBe(0);
    //                 done();
    //             }).catch((e) => done(e));
    //         });
    // });
});





// const text = 'My note test';
// expect(text).toBe('My note test');
// done();