const expect = require('expect');
const request = require('supertest');

const app = require('../server/server');
const Notes = require('../server/models/notes');

const notes = [{
        text: 'First note',
        description: 'First note descriotion'
    },{
        text: 'Second note',
        description: 'Second note descriotion'
}];

beforeEach((done) => {
    Notes.remove({}).then(() => {
        Notes.insertMany(notes);
    }).then(()=> done());
});

describe('GET /notes', () => {
    it('should get all notes', (done) => {        
        request(app)
            .get('/notes')
            .expect(200)
            .expect((res) => {
                expect(res.body.notes.length).toBe(2);
            })
            .end(done);
        });
});