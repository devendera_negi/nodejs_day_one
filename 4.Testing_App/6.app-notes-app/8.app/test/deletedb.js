// const MongoClient = require('mongodb').MongoClient;
const { MongoClient, ObjectID } = require('mongodb');

const obj = new ObjectID();
    
// MongoClient.connect('mongodb://localhost:27017/mynotes', (err, db) => {
    MongoClient.connect('mongodb://devnegi:test123test@ds129386.mlab.com:29386/funchar', (err, db) => {
    if(err) {
        return console.log('Unable to connect to MongoDB server');        
    }
    console.log('connected to MongoDB server');

    // DELETE Many
    // db.collection('notes').deleteMany({
    //         text: "Some notes"
    //     }).then((result) => {
    //     console.log('Delete multipel Notes');
    //     console.log(JSON.stringify(result, undefined, 2));
    // }, (err) => {
    //     console.log('Unable to fetch the dta', err);
        
    // });

    // DELETE one

    // db.collection('user').deleteOne({
    //     name: "Dev Negi"
    // }).then((result) => {
    //     console.log('Delete multipel user');
    //     console.log(JSON.stringify(result, undefined, 2));
    // }, (err) => {
    //     console.log('Unable to fetch the dta', err);        
    // });

    // Find one and Delete one

    // db.collection('user').findOneAndDelete({
    //     status: false
    // }).then((result) => {
    //     console.log('Delete one user');
    //     console.log(JSON.stringify(result, undefined, 2));
    // }, (err) => {
    //     console.log('Unable to fetch the dta', err);        
    // });

    db.collection('user').deleteOne({
        _id: new ObjectID('5a25e0e2c0f9731880bb604e')
    }).then((result) => {
        // console.log('delete user by Id');
        console.log(JSON.stringify(result, undefined, 2));
    }, (err) => {
        console.log('Unable to fetch the dta', err);        
    });

    
    db.close();
});