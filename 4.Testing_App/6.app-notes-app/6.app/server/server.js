const express = require('express');
const bodyParser = require('body-parser');

const  mongoose  = require('./db/connect');
const  Notes = require('./models/notes');
const  User = require('./models/notes');

const app = express();

app.use(bodyParser.json());

app.post('/notes', (req, res) => {
    const notes = new Notes({
        text: req.body.text
    });
    
    notes.save().then((doc) => {
        res.send(doc);  
    }, (e) => {
        res.status(400).send(e);
    })
});

const port = 7777;
app.listen(port, () => {
    console.log(`Server running on port ${port}`);
});

module.exports = app;