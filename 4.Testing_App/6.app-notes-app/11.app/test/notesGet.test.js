const expect = require('expect');
const request = require('supertest');
const { ObjectID } = require('mongodb');

const app = require('../server/server');
const Notes = require('../server/models/notes');


const notes = [{
    _id: new ObjectID(),
    text: 'First note',
    description: 'First note descriotion 001'
},{
    _id: new ObjectID(),
    text: 'Second note',
    description: 'Second note descriotion 002'
}];

beforeEach((done) => {
    Notes.remove({}).then(() => {
        Notes.insertMany(notes);
    }).then(()=> done());
});

describe('GET /notes', () => {
    it('should get all notes', (done) => {        
        request(app)
            .get('/notes')
            .expect(200)
            .expect((res) => {
                expect(res.body.notes.length).toBe(2);
            })
            .end(done);
    });
});

describe('GET /notes/:id', () => {
    it('should return notes doc', (done) => {     
        request(app)
            .get(`/notes/${notes[0]._id.toHexString()}`)
            .expect(200)
            .expect((res) => {
                expect(res.body.note.text).toBe(notes[0].text);
            })
            .end(done);
    });

    it('should return 404 if note not fount', (done) => {
        request(app)
            .get(`/notes/${new ObjectID().toHexString()}`)
            .expect(404)
            .end(done);
    });

    it('should return 404 for non-object ids', (done) => {
        request(app)
            .get(`/notes/${new ObjectID()}212`)
            .expect(404)
            .end(done);
    });
});