/**
 * async fail test will pass
 */
// const expect = require('expect');
// const app = require('./app');

// it('should async add two numbers', () => {
//     app.asyncAdd(4,2, (sum) => {
//         expect(sum).toBe(7).toBeA('number');
//     })
// })

/**
 * using done 
 */

const expect = require('expect');
const app = require('./app');

it('should async add two numbers', (done) => {
    app.asyncAdd(4,2, (sum) => {
        expect(sum).toBe(6).toBeA('number');
        done();
    })
})

/**
 * assignement wirte a async square 
 */