// const utils = require('./utils');
// const { assert } = require('chai');
// it('should add two numbers', () => {
// 	const result = utils.add(33, 11);
	
// 	assert.equal(result, 44);
// 	// expect(result).toBeA('number');
// 	// if(res !== 44) {
// 	// throw new Error(`Expacted 44, but got ${res}`);
// 	// }
// });

// it('should add two nubers and return a number', ()=> {
// 	const result = utils.add(4, 5);
// 	assert.typeOf(result, 'number');
// });


const request = require('supertest');

const app = require('../server').app;

it('should return hello world response', (done) => {
    request(app)
        .get('/')
        .expect('Hello world!')
        .end(done);
})