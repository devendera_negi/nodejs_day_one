const add = (a, b) => a+b;

// module.exports = { add };
module.exports.add = add;

const Person = function (name, age) {
    this.name = name;
    this.age = age;
};

const getPerson = (name, age) => {
    const person = new Person(name, age);
    return person;
}

module.exports.getPerson = getPerson;
