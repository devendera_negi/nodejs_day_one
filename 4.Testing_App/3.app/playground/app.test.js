// const expect = require('expect');

// const app = require('./app');


// it('Should add two numbers', ()=>{
//     const res = app.add(2,3);
//     expect(res).toBe(5).toBeA('number');
// })

// /**
//  * throwing error
//  */



// it('Should run on object', ()=>{
//     const res = {name: 'dev'};
//     // expect(res).toBe({name: 'dev'}); // fail
//     expect(res).toEqual({name: 'dev'});
//     // expect(res).toNotEqual({name: 'dev'});
// })

// it('Should run on array', ()=>{
//     const res = [5,6,7];
//     // expect(res).toInclude(7);
//     expect(res).toExclude(17);
// })


const expect = require('expect');
const {add, getPerson } = require('./app');

it('should have name property ', () => {
    const person = getPerson('Dev', 32);
    expect(person).toInclude({
        name: 'Dev1',
        age: 32
    });
})