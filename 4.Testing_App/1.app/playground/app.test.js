// const app = require('./app');

// it('Should add two numbers', ()=>{
//     const res = app.add(2,3);
//     console.log(res);
// })

/**
 * throwing error
 */

// const app = require('./app');

// it('Should add two numbers', ()=>{
//     const res = app.add(2,3);
//     throw new Error('Test fail');
// })

/**
 * if your add funciton fails
 */

const app = require('./app');

it('Should add two numbers', ()=>{
    const res = app.add(2,3);
    if(res !== 5) {
        throw new Error(`expected 5, but got ${res}`);
    }
});

/**
 * assignment for square function
 */